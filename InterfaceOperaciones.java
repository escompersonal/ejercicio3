public interface InterfaceOperaciones  {
	public int sumar(int a,int b);
	public int restar(int a,int b);
	public int multiplicar(int a,int b);
	public double dividir(double a,double b);	
}