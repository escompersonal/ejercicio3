import java.awt.*;
import javax.swing.*;
import java.awt.event.*;



class PanelOperaciones extends JPanel implements ActionListener{
 	private JLabel JLa;
 	private JLabel JLb;
 	private JTextField JTFa;
 	private JTextField JTFb;
 	private JTextArea JTARes;
 	private JButton BtnCal;

	public PanelOperaciones(){
		JLa = new JLabel();	
		JLb = new JLabel();	
		JTFa = new JTextField();	
		JTFb = new JTextField();
		BtnCal = new JButton();
		JTARes = new JTextArea();
		initComponets();	
	
	}

	public void initComponets(){
		setLayout(null);
		JLa.setText("Valor a:");
		JLb.setText("Valor b:");
		BtnCal.setText("Calcular");

		JLa.setBounds(50,20,60,20);
		JLb.setBounds(50,50,60,20);
		JTFa.setBounds(120,20,100,20);
		JTFb.setBounds(120,50,100,20);
		BtnCal.setBounds(240,30,100,30);
		JTARes.setBounds(25,100,350,120);
		JTARes.setEditable(false);

		add(JLa);
		add(JLb);
		add(JTFa);
		add(JTFb);
		add(BtnCal);
		add(JTARes);

		BtnCal.addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String valA = JTFa.getText();
		String valB = JTFb.getText();
		
		ClienteOperaciones co = new ClienteOperaciones();
		try{
			JTARes.setText("Resultados: \n" + co.enviarDatos(valA + "," + valB));
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
			
	}
}