import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class FrameOperaciones extends JFrame{
	PanelOperaciones po = new PanelOperaciones();

	public FrameOperaciones(){		
		super("Operaciones");
		this.add(po);
		setSize(400,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);	
		setVisible(true);
	}
}